package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {

        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }


    @Test
    public void testSubtraction() throws Exception {

        Double result = calculator.execute("5-2");

        assertEquals("The result of 5-2 must be 3", 3D, (Object)result);
    }

    @Test
    public void testMultiplicatino() throws Exception {

        Double result = calculator.execute("10*5");

        assertEquals("The result of 10*5 must be 50", 50D, (Object)result);
    }

    @Test
    public void testDivision() throws Exception {

        Double result = calculator.execute("8/2");

        assertEquals("The result of 8/2 must be 4", 4D, (Object)result);
    }

    @Test
    public void testPotencia() throws Exception {

        Double result = calculator.execute("2^2");

        assertEquals("The result of 2^2 must be 4", 4D, (Object)result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidInput() throws Exception {

        calculator.execute("sim");

    }
}
